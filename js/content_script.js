$(document).on('page:load', _init_); //此种初始化方式专门针对turbolinks页面:http://www.udpwork.com/item/11531.html

$(document).ready(_init_);

function _init_() {
    //检测是否是code平台
    if (location.origin == "http://code.dianpingoa.com") {

        console.log("codehelper code is running");

        var arr = location.pathname.split('/');

        if (arr.length >= 3) {

            var urlPrefix = location.origin + "/" + arr[1] + "/" + arr[2];
            var namespace = arr[1] + "." + arr[2];

            //初始化ui
            initUI(urlPrefix);

            //检测是否执行了一键发布按钮
            deployCheck(namespace);

            //一键ci
            onekeyCI(urlPrefix, namespace);

            //
            newBranchOrMergeSet(namespace);

            mainCI(urlPrefix);

            //其他设置
            otherSettings(namespace);
        }
    }


    //显示zebra 数据库快捷入口
    if (location.origin == "http://zebra.dp") {
        console.log("codehelper zebra is running");

        var list = get("zebra", 'quickentry');

        if (list) {

            //list=JSON.parse(list);

            var html = '';

            for (var i = 0; i < list.length; i++) {

                if (!list[i].title) {
                    list[i].title = '';
                }

                html += '<li title="' + list[i].title + '" onclick="$(\'li\').removeClass(\'active\');$(this).addClass(\'active\')" class="ng-scope">';
                html += ' <a href="' + list[i].link + '">';
                html += ' <i  class="menu-icon fa fa-database"></i>';
                html += ' <span class="menu-text ng-binding">' + list[i].name + '</span>';
                html += ' </a>';
                html += ' <b class="arrow"></b>';
                html += ' </li>';
                //html+='<li><a href="'+list[i].link+'" target="_blank" >'+list[i].name+'</a></li>';
            }
            //html+='</ul></div>';

            //$(".main-content-inner").prepend(html);

            $(".nav-list").prepend(html);

            $(".sidebar").width(240);
            $(".main-content").css("margin-left", "241px");
        }

        if (location.href.indexOf(':3306') != -1) {
            $("title").text(location.href.substring(location.href.lastIndexOf('/') + 1, location.href.length));

            $("a[href='" + location.href + "'").parent().addClass("active");

        }
    }
}

function mainCI(urlPrefix) {
    //1.ci主界面入口
    if (location.search.indexOf('mainci=true') != -1) {

        $("title").text("一键CI中,请勿操作或关闭此页面!");


        var lightmergebranch = $("option:contains('[beta]')").val();

        location.href = urlPrefix + "/ci_branch/" + lightmergebranch;
    }
}

//新建分支设置
function newBranchOrMergeSet(namespace) {
    if (location.pathname.match("/branches/new$")) {
        var binput = $("#branch_name");
        $("input#ref").val("master");
        binput.val(new Date().Format('yyyyMMdd-'));
        //binput.keyup(function(e){
        //    if(e.keyCode==13){
        //        //写入storage
        //        set(namespace,"new_branch",binput.val());
        //
        //        //回车提交
        //        binput[0].click();
        //    }
        //});

        $("input.btn-create").click(function () {
            //写入storage
            set(namespace, "new_branch", binput.val());

        });


    }

    if (location.pathname.match("/merge_requests/new$")) {
        var targetSelect = $("#merge_request_target_branch").show().val("master");
        targetSelect.next().hide();


        var sourceSelect = $("#merge_request_source_branch").show().val(get(namespace, "new_branch"));
        sourceSelect.next().hide();

        $("#merge_request_title").val(get(namespace, "new_branch"));

    }
}

//一键ci
function onekeyCI(urlPrefix, namespace) {
    //1.入口
    if (location.search.indexOf('onekeyci=true') != -1) {

        console.log("onekeyci start");

        $("title").text("一键CI中,请勿操作或关闭此页面!");


        var lightmergebranch = $("option:contains('[beta]')").val();

        set(namespace, 'onekeyci', lightmergebranch);

        set(namespace, 'onekeyci.start_time', new Date().getTime());


        location.href = urlPrefix + "/ci_branch/" + lightmergebranch;
    }

    var branchname = get(namespace, "onekeyci");

    if (!branchname) {
        console.log("not onekeyci,just ignore!");
        return;
    } else {
        $("title").text("一键CI中,请勿操作或关闭此页面!");
    }

    //2.点击lightmerge按钮
    if (!get(namespace, "onekeyci.has_lightmerge") && location.pathname.match("/ci_branch/" + branchname + "$")) {
        console.log("点击lightmerge按钮");

        set(namespace, "onekeyci.has_lightmerge", true);

        $("a.light-merge-new")[0].click();
    }

    //3.点击Submit Merge Request按钮
    if (location.pathname.match("/light_merges/[0-9]+/edit$")) {
        console.log("点击Submit Merge Request按钮");

        set(namespace, "onekeyci.canpush", true);

        //判断有无newbranch storage
        var new_branch_name = get(namespace, "new_branch");
        if (new_branch_name) {

            //判断是否没有放入到source branches中
            var sourceBranches = $("#light_merge_source_branches");
            if ($.inArray(new_branch_name, sourceBranches.val()) == -1) {
                //将newbranch加入到source  branches中
                sourceBranches.show();
                var arr = sourceBranches.val();
                arr.push(new_branch_name);
                sourceBranches.val(arr);
                $("#light_merge_source_branches_chosen").hide();

            }
        }


        $("input.btn-create")[0].click();
    }

    //4.点击refresh and push
    if (get(namespace, "onekeyci.canpush") && location.pathname.match("/ci_branch/" + branchname + "$")) {
        console.log("点击refresh and push");

        set(namespace, "onekeyci.canpush", false);

        set(namespace, "onekeyci.waitresult", true); //push完后置false

        $("a.lmerge-push")[0].click();


        //将上一次ci push时间存起来
        var lastPushTime = Date.parse($("div.pull-left").find("a#build_status").text()).getTime();
        set(namespace, "onekeyci.last_push_time", lastPushTime);
    }

    //5.刷新结果等待push完成
    if (get(namespace, "onekeyci.waitresult") && location.pathname.match("/ci_branch/" + branchname + "$")) {
        console.log("刷新结果等待push完成");

        setTimeout(function () {
            //检测是否push完成
            var lastPushTime = get(namespace, "onekeyci.last_push_time");

            var time = Date.parse($("div.pull-left").find("a#build_status").text()).getTime();

            //检测是否build失败
            var buildStatusTag = $("span#build_status");
            if (time > lastPushTime && buildStatusTag.text().trim() == 'failure') {

                //中断流程
                console.log("build error");
                set(namespace, 'onekeyci', '');
                set(namespace, "onekeyci.waitresult", false);
                set(namespace, "onekeyci.has_lightmerge", false);

                //发送桌面通知
                NotificationAPI.sendTextNotice(namespace, "项目beta环境build失败!!!");

                $("title").text("一键beta失败,请手动处理!");

                return;
            }


            if (time > lastPushTime && buildStatusTag.text().trim() == 'success' && $("div.bar").attr('style').split(':')[1] == "100%") {
                //push完成
                set(namespace, "onekeyci.waitresult", false);

                console.log("push完成");


                //6.点击部署按钮
                var deploy_button = $("button:contains('部署')");
                deploy_button[0].click();

                //等待部署完成
                //set(namespace, "onekeyci.wait_all_completed", true);
                //
                ////刷新页面
                //location.reload()

                var stamp = setInterval(function () {
                    //var deploy_button=$("button:contains('部署')");
                    var deployStatus = $("span.deploystatus").last();
                    var deploySuccess = false;
                    if (deployStatus.parent().text().trim().indexOf("(paas)") != -1) {
                        //paas机
                        if (deployStatus.text().trim() == "deploy success") {
                            deploySuccess = true;
                        }

                    } else {
                        //检测是否部署完成
                        if (!deploy_button.attr('disabled')) {
                            deploySuccess = true;
                        }
                    }

                    console.log("deploy status:" + deploySuccess);

                    if (deploySuccess) {

                        //set(namespace, "onekeyci.wait_all_completed", false);
                        clearInterval(stamp);

                        //部署完成
                        console.log("部署完成");

                        set(namespace, 'onekeyci', '');
                        set(namespace, "onekeyci.has_lightmerge", false);

                        //发送桌面通知
                        var startTime = get(namespace, 'onekeyci.start_time');
                        NotificationAPI.sendTextNotice(namespace, "项目beta环境已部署完成!共计耗时:" + parseInt((new Date().getTime() - startTime) / 1000) + "秒");

                        $("title").text("一键CI部署完成,可以进行其他操作了!");

                    }
                    /*else{
                     location.reload();
                     }*/

                }, 3000);


            } else {
                location.reload()
            }
        }, 10000);

    }

    //6.刷新结果等待部署完成
    //if(get(namespace, "onekeyci.wait_all_completed") && location.pathname.match("/ci_branch/" + branchname + "$")){
    //    console.log("刷新结果等待deploy完成");
    //
    //
    //    setTimeout(function () {
    //        var deploy_button=$("button:contains('部署')");
    //        var deployStatus = $("span.deploystatus").last();
    //        var deploySuccess = false;
    //        if (deployStatus.parent().text().trim().indexOf("(paas)") != -1) {
    //            //paas机
    //            if (deployStatus.text().trim() == "deploy success") {
    //                deploySuccess = true;
    //            }
    //
    //        } else {
    //            //检测是否部署完成
    //            if (!deploy_button.attr('disabled')) {
    //                deploySuccess = true;
    //            }
    //        }
    //
    //        console.log("deploy status:"+deploySuccess);
    //
    //        if (deploySuccess) {
    //
    //            set(namespace, "onekeyci.wait_all_completed", false);
    //
    //            //部署完成
    //            console.log("部署完成");
    //
    //            set(namespace, 'onekeyci', '');
    //            set(namespace, "onekeyci.has_lightmerge", false);
    //
    //            //发送桌面通知
    //            var startTime=get(namespace, 'onekeyci.start_time');
    //            NotificationAPI.sendTextNotice(namespace, "项目beta环境已部署完成!共计耗时:"+parseInt((new Date().getTime()-startTime)/1000)+"秒");
    //
    //            $("title").text("一键CI部署完成,可以进行其他操作了!");
    //
    //        }else{
    //            location.reload();
    //        }
    //
    //    },15000);
    //}

}


//其他设置
function otherSettings(namespace) {



    //窗口关闭绑定事件
    //if(get(namespace,"onekeydeploy")){
    //    window.onbeforeunload = function(event){
    //        if(event.clientX>document.body.clientWidth && event.clientY<0 ||event.altKey){
    //            return "一键上线正在进行中,确认要关闭吗?"
    //        }else{
    //            //window.event.returnValue="";
    //            //return false;
    //        }
    //
    //    };
    //    window.onunload = function(event){
    //        if(event.clientX>document.body.clientWidth && event.clientY<0 ||event.altKey){
    //            set(namespace,'onekeydeploy',false);
    //        }
    //
    //    };

    //}

    //chrome.windows.onRemoved.addListener(function(windowId){
    //    alert(windowId);
    //});


}


//初始化ui
function initUI(urlPrefix) {

    console.log("initUI");


    var html = '<div style="margin-bottom: 10px;margin-right: 10px;margin-top: 10px;/* margin: auto; */text-align: center;vertical-align: middle;font-size: 18px;"><span>快捷操作:</span>';
    html += '<a style="margin-right:10px;margin-left: 10px;" href="' + urlPrefix + '/ci_branch/master?mainci=true" >CI主界面</a>';
    html += '<a style="margin-right:10px;" href="' + urlPrefix + '/ci_branch/master?onekeyci=true" >一键beta</a>';
    html += '<a style="margin-right:10px;" href="' + urlPrefix + '/merge_requests/new" >new_merge</a>';
    html += '<a style="margin-right:10px;" href="' + urlPrefix + '/rollout_branches/new?onekeydeploy=true">一键上线</a>';
    html += '<a style="margin-right:10px;" href="' + urlPrefix + '/branches/new" >new_branch</a>';
    html += '<a style="margin-right:10px;" href="http://code.dianpingoa.com/dashboard/merge_requests" >merge_requests</a>';

    html += '<hr style="margin:10px 0;border-top: 1px solid #24901C;"></div>';

    $(".main-nav").prepend(html);


}

function deployCheck(namespace) {
    try {
        console.log("deployCheck");

        oneKeyDeploy(namespace);
    } catch (err) {
        console.error(err);
        set(namespace, 'onekeydeploy', false);
    }
}


//一键部署上线检测
function oneKeyDeploy(namespace) {



    //自动打包- 引子
    if (!get(namespace, "onekeydeploy") && location.search.indexOf('onekeydeploy=true') != -1) {

        set(namespace, 'onekeydeploy.start_time', new Date().getTime());

        console.log("oneKeyDeploy start");

        set(namespace, 'onekeydeploy', true);

        //set(namespace,'current_tab_id',current_tab_id);

        var tag_name=new Date().Format("yyyyMMddHHmmss");
        $("#packing_tag").val(tag_name);

        //
        //if(namespace.indexOf('job')!=-1){
        //    var configStr=get(namespace,'config');
        //    if(configStr){
        //        $.get('http://code.dianpingoa.com/'+namespace.replace('.','/')+'/online_jobs/tags_for_jar?env=&branch_name=master&jar_name='+namespace.split('.')[1],function(data){
        //            var arr=configStr.split(',');
        //            arr[2]=data[0].id;
        //            set(namespace,'config',arr.join(','));
        //        });
        //
        //
        //    }
        //}

        setTimeout(function () {
            var select = $("#packing_module_names");
            select.show();
            $("#packing_module_names_chosen").hide();
            $("#packing_note").val("codehelper");

            //自动选择要打的模块
            select.children().each(function (i, option) {
                var maybeModule = option.value.indexOf('service') != -1 || option.value.indexOf('server') != -1
                    || option.value.indexOf('web') != -1 || option.value.indexOf('mq') != -1 || option.value.indexOf('job') != -1;
                if (maybeModule) {
                    select.val(option.value);

                    //build按钮
                    $("input[type='submit']").click();

                    return;
                }
            })

        }, 30);
    }


    //检测是否有设置一键部署操作
    if (!get(namespace, "onekeydeploy")) {
        console.log(("not onekeydeploy,just ingore!"));
        return;
    }


    $("title").text("一键上线中,请勿操作或关闭此页面!");


    //打包进行中
    if (location.pathname.match("/rollout_branches/[0-9]+")) {
        console.log("打包进行中");

        //发现有"请允许发布",自动点
        var stamp = setInterval(function () {
            var linkCreate = $(".btn-create");
            if (linkCreate[0]) {
                clearInterval(stamp);
                location.href = linkCreate.attr("href");
            }
        }, 1000);
    }


    //包列表页面,自动点击最上面的"发布"
    if (location.pathname.match("/rollout_branches$")) {
        console.log("打包完成,点击发布");

        var a = $("a[data-method='post']")[0];
        if (a) {
            a.click();
        } else {
            $("#packing-list").find("tr").eq(1).find("a").last()[0].click()
        }

    }

    //job发布
    if (location.pathname.match("/online_jobs/new")) {
        console.log("job发布");

        //从storage读取默认配置
        var defaultConfig=get(namespace,"config");

        if(defaultConfig) {
            var vals= defaultConfig.split(',');

            $.get('http://code.dianpingoa.com/'+namespace.replace('.','/')+'/online_jobs/tags_for_jar?env=&branch_name=master&jar_name='+namespace.split('.')[1],function(data){
                vals[2]=data[0].id;

                //选择默认select框
                $("#online_job_build_branch_id").val(vals[0]);

                $("#online_job_jar_name").append("<option value='"+vals[1]+"'>"+vals[1]+"</option>").val(vals[1]);

                $("#online_job_tag").append("<option value='"+vals[2]+"'>"+vals[2]+"</option>").val(vals[2]);

                $("#online_job_env").val(vals[3]);

                $("#online_job_business_belongs_to").append("<option value='"+vals[2]+"'>"+vals[2]+"</option>").val(vals[4]);

                $("#online_job_machine").append("<option value='"+vals[5]+"'>"+vals[5]+"</option>").val(vals[5]);


                //点击deploy按钮
                $("input[type='submit']")[0].click();


                $("title").text("一键上线完成,可以进行其他操作了");

                set(namespace, 'onekeydeploy', false);

                //发送桌面通知
                var startTime = get(namespace, 'onekeydeploy.start_time');

                NotificationAPI.sendTextNotice(namespace, "项目product环境已全部部署完成!共计耗时:" + parseInt((new Date().getTime() - startTime) / 1000) + "秒");
            });


        }else{
            //显示保存配置按钮
            var html='<input class="btn btn-primary" type="button" value="保存配置" style="margin-right: 10px;background: green;" id="saveConfig">';
            $(".form-actions").prepend(html);

            $("#saveConfig").click(function(){
                var configStr="";
                configStr+=$("#online_job_build_branch_id").val()+",";

                configStr+=$("#online_job_jar_name").val()+",";

                configStr+=$("#online_job_tag").val()+",";

                configStr+=$("#online_job_env").val()+",";

                configStr+=$("#online_job_business_belongs_to").val()+",";

                configStr+=$("#online_job_machine").val();

                set(namespace,"config",configStr);

            });

            $("title").text("job部署完成,请选择上线机器!");
            set(namespace, 'onekeydeploy', false);

            //发送桌面通知
            var startTime = get(namespace, 'onekeydeploy.start_time');

            NotificationAPI.sendTextNotice(namespace, "项目product环境已全部部署完成,请确认线上机器,配置好后下次将按配置自动发布!本次共计耗时:" + parseInt((new Date().getTime() - startTime) / 1000) + "秒");
        }

    }


    //自动部署,非job项目
    if (location.pathname.match("/tickets/[0-9]+")) {
        console.log("自动部署上线");

        var selectTag = $("#Tag");
        var datalinktr = $("tr[data-link]");
        selectTag.val(selectTag.children()[1].value);

        setTimeout(function () {

            var script = document.createElement("script");
            script.type = "text/javascript";
            script.innerText = '$("#Tag").attr("data_codehelper",window.gon.api_token)';
            var Head = document.getElementsByTagName('body').item(0);
            Head.appendChild(script);

            //update_package
            var url = selectTag.children()[1].value;
            var package_id = datalinktr.attr("data-link").split("?")[1].split("&")[0].split("=")[1];
            var private_token = selectTag.attr("data_codehelper");
            $.post("http://code.dianpingoa.com/api/v3/ci/update_package", {
                'url': url,
                'package_id': package_id,
                'private_token': private_token
            });

            //改变data-link
            datalinktr.attr("data-link", datalinktr.attr("data-link").split("?")[0] + "?package_id=" + package_id + "&tag=" + selectTag.children()[1].text);

            datalinktr.click();

            setTimeout(function () {
                var groups = $("button.info_to_verify");
                if (groups.size()) {
                    var totalGroups = $("tr.grouping-item-info").size();
                    var deployedGroups = 0;
                    //一组一组的上
                    groups[0].click();

                    setTimeout(function () {
                        //点击确认按钮
                        $("button.release-deploy").click();
                    }, 1000);


                    //开始检测该组是否已完成
                    var ss = setInterval(function () {
                        var items = $("tr.grouping-item-info");
                        if (items.eq(deployedGroups).children().eq(2).text().indexOf('Deploy Success') != -1) {
                            //已完成
                            console.log("group completed!");

                            deployedGroups++;

                            if (deployedGroups == totalGroups) {
                                //all completed
                                console.log("all groups completed!");
                                console.log("onekeydeloy completed!");
                                $("title").text("一键上线完成,可以进行其他操作了!");

                                set(namespace, 'onekeydeploy', false);

                                //发送桌面通知
                                var startTime = get(namespace, 'onekeydeploy.start_time');

                                NotificationAPI.sendTextNotice(namespace, "项目product环境已全部部署完成!共计耗时:" + parseInt((new Date().getTime() - startTime) / 1000) + "秒");

                                clearInterval(ss);
                            } else {

                                //下一组点击发布按钮
                                //一组一组的上
                                var nextGroup = items.eq(deployedGroups).children().eq(3).children().eq(0).children().eq(0);
                                nextGroup.click();

                                setTimeout(function () {
                                    //点击确认按钮
                                    $("button.deploy_check_btn").click();
                                    //二次确认
                                    setTimeout(function () {
                                        $("button.release-deploy").click();
                                    }, 1000);
                                }, 1000);

                            }

                        }


                    }, 3000);
                }


            }, 1000)
        }, 30);

        //var stamp=setInterval(function(){
        //    datalinktr.click();
        //},7000);

    }


}


function get(namespace, name) {
    var ret = localStorage[namespace + "." + name];
    if (ret) {
        if (ret.indexOf('s|') != 0) {
            //string
            ret = JSON.parse(ret);
        } else {
            ret = ret.substring(2, ret.length);
        }
    }

    return ret;
}

function set(namespace, name, value) {
    if (typeof(value) == 'string') {
        value = 's|' + value;
    }
    localStorage[namespace + "." + name] = value;
}


//chrome.tabs.getSelected(function(tab){current_tab_id = tab.id;});

// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
// 例子：
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18
Date.prototype.Format = function (fmt) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "H+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
};

//yyyy-MM-dd_HH-mm-ss
Date.parse = function (str) {
    var arr = str.split('_');
    var ymd = arr[0].split('-');
    var hds = arr[1].split('-');

    var date = new Date(ymd[0], parseInt(ymd[1]) - 1, ymd[2], hds[0], hds[1], hds[2]);

    return date;
};