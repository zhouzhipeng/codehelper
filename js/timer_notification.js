﻿/**
 * 消息通知对象，用于发送桌面通知
 *  // 注意：没有必要调用 webkitNotifications.checkPermission()。
 // 声明了 notifications 权限的扩展程序总是允许创建通知。

 // 创建一个简单的文本通知：
 var notification = webkitNotifications.createNotification(
 '48.png',  // 图标 URL，可以是相对路径
 '您好！',  // 通知标题
 '内容（Lorem ipsum...）'  // 通知正文文本
 );

 // 或者创建 HTML 通知：
 var notification = webkitNotifications.createHTMLNotification(
 'notification.html'  // HTML 的 URL，可以是相对路径
 );

 // 然后显示通知。
 notification.show();
 */
var NotificationAPI = (NotificationAPI !== undefined) ? NotificationAPI : {};

(function (n) {
    HTMLAudioElement.prototype.stop = function()
    {
        this.pause();
        this.currentTime = 0.0;
    };
    var audio = new Audio('http://code.dianpingoa.com/beauty/tech-labs/raw/master/code-helper/3462.wav');
    audio.loop = "loop";

    function initNotification(title,content){
        var  notification = new Notification(title,{
            icon:'http://code.dianpingoa.com/assets/logo-new.png',
            body:content
        });

        //绑定监听事件
        notification.onshow = function () {
            audio.play();
            console.log("ondisplay");
        };
        notification.onclose = function () {
            console.log("onclose");
            audio.stop();
        };
        notification.onclick = function () {
            console.log("onclick");
            audio.stop();
        };
        notification.onerror = function () {
            console.log("onerror");
            audio.stop();
        };
    }


    //发送普通文本通知
    n.sendTextNotice = function (title, content) {
        ////额外的扩展需求
        //if(title.indexOf("微小宝")!=-1){
        //    chrome.windows.create({url:content},function(w){
        //        chrome.windows.update(w.id,{
        //            /*
        //             left ( optional integer 可选，整数 )
        //             视窗相对屏幕左边界进行移动的像素偏移值。
        //             top ( optional integer 可选，整数 )
        //             视窗相对屏幕上边界进行移动的像素偏移值。
        //             width ( optional integer可选，整数 )
        //             视窗宽度调整的像素值。
        //             height ( optional integer可选，整数 )
        //             视窗高度调整的像素值。
        //             focused ( optional boolean可选，Boolean类型 )
        //             如果是true，将该视窗提至前面。否则，将z-order上下一视窗提至前面。
        //             */
        //
        //            left:0,
        //            top:0,
        //            width:screen.availWidth,
        //            height:screen.availHeight,
        //            focused:true
        //        })
        //    });
        //}
        // Let's check if the browser supports notifications
        if (!("Notification" in window)) {
            alert("浏览器不支持桌面通知!建议使用chrome内核的浏览器!");
        }

        // Let's check if the user is okay to get some notification
        else if (Notification.permission === "granted") {
            // If it's okay let's create a notification

            initNotification(title,content);
        }
        else{
            Notification.requestPermission(function (permission) {
                // If the user is okay, let's create a notification
                if (permission === "granted") {
                    initNotification(title,content);
                }
            });
        }
    };

})(NotificationAPI);

Object.preventExtensions(NotificationAPI);