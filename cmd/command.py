#!/usr/bin/env python
#-*- coding:UTF-8 -*-
import sys
import webbrowser
import re
#sys.path.append("libs")


def open_url(url):
    webbrowser.open(url,new=1)

if __name__ == '__main__':
    filepath=sys.argv[1]
    #filepath='/Users/zhouzhipeng/code/company_projects/20160420-newcouponservice/beauty-campaign-web'
    filepath+='/.git/config'
    content=open(filepath).read()
    result=re.search('url = git@code\.dianpingoa\.com:([a-zA-Z0-9-]+)/([a-zA-Z0-9-]+)\.git',content)
    if result:
        group_name=result.group(1)
        project_name=result.group(2)

        if sys.argv[2]=='beta':
            open_url('http://code.dianpingoa.com/%s/%s/ci_branch/master?onekeyci=true' %(group_name,project_name))
        elif sys.argv[2]=='product':
            open_url('http://code.dianpingoa.com/%s/%s/rollout_branches/new?onekeydeploy=true' %(group_name,project_name))
    else:
        print("it's not code.dianpingoa.com git repository:"+filepath)